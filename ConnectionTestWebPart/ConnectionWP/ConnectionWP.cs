﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace ConnectionTestWebPart.ConnectionWP
{
	/// <summary>
	/// This will create the connection between the provider and the consumer (this class).
	/// This will save the row of data being passed in to the "tableData" row.
	/// This will create the child controls and pass in a reference to itself to the child control so that
	/// the child control can access this.tableData.
	/// </summary>
	public class ConnectionWP : WebPart
	{
		// Visual Studio might automatically update this path when you change the Visual Web Part project item.
		private const string _ascxPath = @"~/_CONTROLTEMPLATES/ConnectionTestWebPart/ConnectionWP/ConnectionWPUserControl.ascx";

		/// <summary>
		/// Used as the link to the Providing WebPart row
		/// </summary>
		private IWebPartRow _provider;

		/// <summary>
		/// Storage of data from Provider
		/// </summary>
		internal DataRowView tableData;

		/// <summary>
		/// Create Child controls
		/// </summary>
		/// <remarks>
		/// Notice that we are casting the child control here to be specific. We have to do this because we want
		/// to save the child controls "WebPart" property to reference THIS CLASS. This is because we want to
		/// reference this.tableData from the child class so that we can have access to the row of data from the
		/// child class and be able to place that data in a ASP.NET control
		/// </remarks>
		protected override void CreateChildControls()
		{
			ConnectionWPUserControl control = Page.LoadControl(_ascxPath) as ConnectionWPUserControl;
			control.WebPart = this;
			Controls.Add(control);
		}

		/// <summary>
		/// Retrieve object being passed in by Provider and save to instance variable
		/// </summary>
		/// <param name="rowData">Row of Data as DataRowView</param>
		private void GetRowData(object rowData)
		{
			tableData = rowData as DataRowView;
		}

		/// <summary>
		/// Get the row from the provider 
		/// </summary>
		protected override void OnPreRender(EventArgs e)
		{
			if (_provider != null)
			{
				_provider.GetRowData(new RowCallback(GetRowData));
			}
		}

		/// <summary>
		/// Sets a reference to the Provider in an instance variable
		/// </summary>
		[ConnectionConsumer("Row")]
		public void SetConnectionInterface(IWebPartRow provider)
		{
			_provider = provider;
		}
	}
}
