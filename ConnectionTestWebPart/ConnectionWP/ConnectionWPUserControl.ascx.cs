﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace ConnectionTestWebPart.ConnectionWP
{
	/// <summary>
	/// Populates an ASP.NET control with data it recieved from its parent control
	/// </summary>
	/// <remarks>
	/// Note that retrieving the data must happen in OnPreRender method
	/// </remarks>
	public partial class ConnectionWPUserControl : UserControl
	{
		/// <summary>
		/// Reference to the Parent WebPart
		/// </summary>
		internal ConnectionWP WebPart { get; set; }

		/// <summary>
		/// Fill the controls with proper data from the passed in control
		/// </summary>
		/// <remarks>
		/// Note that the names of the columns are the ACTUAL SharePoint column names, not the apparent names
		/// </remarks>
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			this.myLitFileName.Text = this.WebPart.tableData["LinkFilename"].ToString();
			this.myOrderId.Text = this.WebPart.tableData["OrderId"].ToString();
		}
	}
}
